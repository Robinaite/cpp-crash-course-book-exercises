/**
 * 13-5. Consider the following program that profiles the performance of a func-tion summing a Fibonacci series:
This program contains a computationally intensive function fib_sumu that computes the sum of a Fibonacci series with a given length. 
Adapt your code from Exercise 13-1 by (a) generating the appropriate vector and (b) summing over the result with a range-based for loop. 
The random function v returns a ran-dom number between 1,000 and 2,000, 
and the Stopwatch classw adopted from Listing 12-25 in Chapter 12 helps you determine elapsed time. 
In the program’s main, you perform a million evaluations of the fib_sum function using random input y. 
You time how long this takes and print the result before exiting the program 
Compile the program and run it a few times to get an idea of how long your program takes to run. (This is called a baseline.)
13-6. Next, comment out y and uncomment z. Implement the function cached _fib_sumx so you first check whether you’ve computed fib_sum for the given length yet. 
(Treat the length n as a key into the cache.) 
If the key is present in the cache, simply return the result. 
If the key isn’t present, compute the correct answer with fib_sum, store the new key-value entry into cache, and return the result. 
Run the program again. Is it faster? Try unordered_map instead of map. Could you use a vector instead? How fast can you get the program to run?
* */

#include <chrono>
#include <cstdio>
#include <random>
#include <map>
#include <vector>

long FibSum(size_t n)
{

  long sum{};
  long startNum{};
  long nextNum{1};
  for (size_t i{}; i < n; i++)
  {
    long nextVal = startNum + nextNum;
    startNum = nextNum;
    nextNum = nextVal;
    sum += nextVal;
  }
  return sum;

  //this method the exercise requires is just plainly not optimal as it requires saving all the fib numbers when not needed as result is only sum.
  //Above is much quicker and requires less memory;
  // std::vector<long> fib{0,1};
  // for(size_t i{1}; i<n;i++){
  //   fib.push_back(fib[i] + fib[i-1]);
  // }
  // long sum{};
  // for(long fibNum : fib){
  //   sum += fibNum;
  // }

  // return sum;
};
long Random()
{
  static std::mt19937_64 mtEngine{102787};
  static std::uniform_int_distribution<long> intD{1000, 2000};
  return intD(mtEngine);
};

struct Stopwatch
{
  Stopwatch(std::chrono::nanoseconds &result) : result{result}, start{std::chrono::system_clock::now()} {}
  ~Stopwatch()
  {
    result = std::chrono::system_clock::now() - start;
  }

private:
  std::chrono::nanoseconds &result;
  const std::chrono::time_point<std::chrono::system_clock> start;
};

long CachedFibSum(size_t n)
{
  static std::map<long, long> cache;
  if (cache.count(n) > 0)
  {
    return cache[n];
  }
  else
  {
    cache[n] = FibSum(n);
  }
  return cache[n];
};

int main(int argc, char **argv)
{
  size_t samples{1'000'000};
  std::chrono::nanoseconds elapsed;
  {
    Stopwatch stopwatch{elapsed};
    volatile double answer;
    while (samples--)
    {
      //answer = FibSum(Random());
      answer = CachedFibSum(Random());
    }
  }
  printf("Elapsed: %g s.\n", elapsed.count() / 1'000'000'000.);
}