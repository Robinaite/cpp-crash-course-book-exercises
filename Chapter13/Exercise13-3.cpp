/**
13-3. Write a program that accepts any number of command line arguments and prints them in alphanumerically sorted order. 
Use a std::set<const char*>to store the elements, then iterate over the set to obtain the sorted result. 
You’ll need to implement a custom comparator that compares two C-style strings.
* */

#include <iostream>
#include <set>

int main(int argc, char **argv)
{
  auto cmp = [](const char *a, const char *b) {
    size_t i{};
    while (const auto charA = a[i])
    {
      if (const auto charB = b[i])
      {
        return charA < charB;
      }
      else
      {
        break;
      }
    }
    return true;
  };
  std::set<char *, decltype(cmp)> args(cmp);
  for (size_t i{1}; i < argc; i++)
  {
    args.insert(argv[i]);
  }

  for (const char *arg : args)
  {
    std::cout << arg << std::endl;
  }
}