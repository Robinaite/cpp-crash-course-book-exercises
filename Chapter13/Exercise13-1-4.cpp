/**
13-1. Write a program that default constructs a std::vector of unsigned longs. Print the capacity of vector and then reserve 10 elements. 
Next, append the first 20 elements of the Fibonacci series to the vector. Print capacity again. Does capacity match the number of elements in the vector? 
Why or why not? Print the elements of vector using a range-based for loop. 
* */

#include <vector>
#include <iostream>

int main()
{
  std::vector<unsigned long> uLongs;
  std::cout << uLongs.capacity() << std::endl;
  uLongs.reserve(10);

  uLongs.assign({0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181});
  std::cout << uLongs.capacity() << std::endl;

  for (unsigned long el : uLongs)
  {
    std::cout << el << std::endl;
  }

  return 0;
}