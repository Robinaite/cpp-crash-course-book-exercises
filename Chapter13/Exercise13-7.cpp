/**
13-7. Implement a Matrix class like SquareMatrix in Listing 13-38. ??? SHould probably be Listing 13-40;
Your Matrixshould allow unequal numbers of rows and columns. 
Accept as your construc-tor’s first argument the number of rows in Matrix.
* */

#include <cmath>
#include <stdexcept>
#include <initializer_list>
#include <vector>
#include <iostream>

size_t ColumnsAmount(size_t valSize, size_t rowNum)
{
  if (rowNum == 0 || valSize % rowNum != 0)
  {
    throw std::logic_error{"Not enough to fill all rows."};
  }

  return valSize / rowNum;
};

template <typename T>
struct WeirdMatrix
{
  WeirdMatrix(size_t rows, std::initializer_list<T> val)
      : maxRows{rows}, maxColumns{ColumnsAmount(val.size(), rows)}, data(rows, std::vector<T>{})
  {
    auto itr = val.begin();
    for (size_t row{}; row < maxRows; row++)
    {
      data[row].assign(itr, itr + maxColumns);
      itr += maxColumns;
    }
  }

  T &at(size_t row, size_t col)
  {
    if (row >= maxRows || col >= maxColumns)
    {
      throw std::out_of_range{"Index invalid."};
    }
    return data[row][col];
  }
  const size_t maxRows;
  const size_t maxColumns;

private:
  std::vector<std::vector<T>> data;
};

int main()
{

  std::initializer_list<int> values{
      1, 2, 3, 4, 5, 6,
      7, 8, 9, 10, 11, 12};
  WeirdMatrix mat{2, values};
  std::cout << mat.at(1, 2) << std::endl;
}