You can find all the exercises I have done of the book per chapter.

The book:

C++ Crash Course
A Fast-Paced Introduction
by Josh Lospinoso
September 2019, 792 pp.
ISBN-13: 978-1-59327-888-5

Link: https://nostarch.com/cppcrashcourse
