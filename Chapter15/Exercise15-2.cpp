
/**
15-2. Implement a program that determines whether the user’s input is a palindrome.
 * */

#include <iostream>
#include <string>

int main(int argc, char **argv)
{
    std::string pali;
    for (size_t i{1}; i < argc; i++)
    {
        pali += argv[i];
    }

    auto itBegin = pali.begin();
    auto itEnd = pali.end();
    itEnd--;
    while (itBegin != itEnd)
    {
        if (*itBegin != *itEnd)
        {
            std::cout << pali << " is not a Palindrome." << std::endl;
            return 0;
        }
        itBegin++;
        itEnd--;
    }
    std::cout << pali << " is a Palindrome." << std::endl;
}