
/**
15-3. Implement a program that counts the number of vowels in the user’s input
 * */

#include <iostream>
#include <string>
#include <regex>

int main(int argc, char **argv)
{
    std::string str;
    for (size_t i{1}; i < argc; i++)
    {
        str += argv[i];
    }

    std::regex reg{"[AEIOUaeoiu]"};
    std::smatch vowelsMatch;
    auto match = std::sregex_iterator(str.begin(),str.end(),reg);
    std::cout << std::distance(match,std::sregex_iterator()) << std::endl;
}