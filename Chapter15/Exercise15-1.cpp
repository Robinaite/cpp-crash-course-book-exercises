
/**
15-1. Refactor the histogram calculator in Listings 9-30 and 9-31 to use std::string. 
Construct a string from the program’s input and modify AlphaHistogram to accept a string_view or a conststring& in its ingestmethod. 
Use a range-based for loop to iterate over the ingested elements ofstring. Replace the counts field’s type with an associative container.
 * */

#include <cstdint>
#include <cstdio>
#include <iostream>
#include <string>
#include <map>

constexpr char pos_A{65}, pos_Z{90}, pos_a{97}, pos_z{122};
constexpr bool within_AZ(char x)
{
    return pos_A <= x && pos_Z >= x;
}
constexpr bool within_az(char x)
{
    return pos_a <= x && pos_z >= x;
}

struct AlphaHistogram
{
    void ingest(const std::string &x);
    void print() const;

private:
    std::map<char,size_t> counts;
};

void AlphaHistogram::ingest(const std::string &x)
{
    for(const auto c : x){
        if (within_AZ(c))
            counts[c]++;
        else if (within_az(c))
            counts[c]++;
    }
}

void AlphaHistogram::print() const
{
    for (std::pair<char,size_t> p : counts)
    {
        printf("%c: ", p.first);
        auto n_asterisks = p.second;
        while (n_asterisks--)
            printf("*");
        printf("\n");
    }
}

int main(int argc, char **argv)
{
    AlphaHistogram hist;
    for (size_t i{1}; i < argc; i++)
    {
        hist.ingest(argv[i]);
    }
    hist.print();
}