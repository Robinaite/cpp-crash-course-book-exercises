
/**
15-4. Implement a calculator program that supports addition, subtraction, multi-plication, and division of any two numbers. 
Consider using the find method of std::string and the numeric conversion functions.
15-5. Extend your calculator program in some of the following ways: 
permit multiple operations or the modulo operator and accept floating-point numbers or parentheses.
 * */

#include <iostream>
#include <string>
#include <regex>
// this is a super simple calculator, allows for multiple operations, but doesn't adhere to the rules of * and / first and then - and +; 
// Also misses input validations and division by 0 checks, but not enough time unfortunately
int main(int argc, char **argv)
{
    std::string str;
    for (size_t i{1}; i < argc; i++)
    {
        str += argv[i];
    }

    std::vector<std::string> values;
    std::vector<std::string> symbols;

    std::regex reg{"(\\+|-|\\*|\\/)"};
    std::smatch mResults;
    bool match = std::regex_search(str, mResults, reg);
    values.push_back(mResults.prefix());
    symbols.push_back(mResults[1].str());
    std::string subStr{mResults.suffix().str()};
    do
    {
        match = std::regex_search(subStr, mResults, reg);
        if (match)
        {
            values.push_back(mResults.prefix());
            symbols.push_back(mResults[1].str());
            subStr = mResults.suffix().str();
        }
        else
        {
            values.push_back(subStr);
        }
    } while (match);


    double startVal = std::stod(values[0]);
    for (size_t i{}; i < symbols.size(); i++) 
    {
        double nextVal = std::stod(values[i+1]);
        if(symbols[i] == "+"){
            startVal += nextVal;
        } else if(symbols[i] == "-"){
            startVal -= nextVal;
        }else if(symbols[i] == "*"){
            startVal *= nextVal;
        }else if(symbols[i] == "/"){ 
            startVal /= nextVal;
        }
    }
    std::cout << startVal << std::endl;
}