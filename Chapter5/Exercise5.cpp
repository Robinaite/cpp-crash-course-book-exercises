#include <iostream>
#include <map>
/**
 * 5-1. You didn’t implement an accounting system in your Bank. 
 * Design an inter-face called AccountDatabase that can retrieve and set amounts in bank accounts (identified by a long id).
 * 5-2. Generate an InMemoryAccountDatabase that implements AccountDatabase.
 * 5-3. Add an AccountDatabase reference member to Bank. Use constructor injec-tion to add an InMemoryAccountDatabase to the Bank.
 * 5-4. Modify ConsoleLogger to accept a const char* at construction. When ConsoleLogger logs, prepend this string to the logging output. 
 * Notice that you can modify logging behavior without having to modify Bank.
* */

struct ILogger
{
    virtual ~ILogger() = default;
    virtual void LogTransfer(long from, long to, double amount) = 0;
};

struct ConsoleLogger : ILogger
{
    ConsoleLogger(const char *prepend) : prepend{prepend}
    {
    }
    void LogTransfer(long from, long to, double amount) override
    {
        std::cout << prepend << " " << from << " - " << to << " - " << amount << std::endl;
    }

private:
    const char *prepend;
};

struct FileLogger : ILogger
{
    void LogTransfer(long from, long to, double amount) override
    {
        std::cout << "[File] " << from << " - " << to << " - " << amount << std::endl;
    }
};

struct IAccountDatabase
{
    ~IAccountDatabase() = default;
    virtual double RetrieveAmount(long &id) = 0;
    virtual void SetAmount(long &id, double amount) = 0;
};

struct InMemoryAccountDatabase : IAccountDatabase
{
    InMemoryAccountDatabase() = default;
    double RetrieveAmount(long &id)
    {
        try
        {
            return accounts.at(id);
        }
        catch (const std::out_of_range &e)
        {
            return 0;
        }
    }
    void SetAmount(long &id, double amount)
    {
        accounts[id] = amount;
    }

private:
    std::map<long, double> accounts;
};

struct Bank
{
    Bank(ILogger *newLogger, IAccountDatabase *newAccountDatabase) : logger{newLogger}, accountDatabase{newAccountDatabase}
    {
    }
    void SetLogger(ILogger *newLogger)
    {
        logger = newLogger;
    }
    void MakeTransfer(long from, long to, double amount)
    {
        if (logger)
        {
            logger->LogTransfer(from, to, amount);
        }
        accountDatabase->SetAmount(from, accountDatabase->RetrieveAmount(from) - amount);
        accountDatabase->SetAmount(to, accountDatabase->RetrieveAmount(to) + amount);

        std::cout << accountDatabase->RetrieveAmount(from) << " / " << accountDatabase->RetrieveAmount(to) << std::endl;
    }

private:
    ILogger *logger;
    IAccountDatabase *accountDatabase;
};

int main()
{
    ConsoleLogger cLogger{"[Console]"};
    FileLogger fLogger;
    InMemoryAccountDatabase memAccDB;
    Bank bank{&cLogger, &memAccDB};
    bank.MakeTransfer(1, 2, 100);
    bank.SetLogger(&fLogger);
    bank.MakeTransfer(2, 1, 100);
    return 0;
}