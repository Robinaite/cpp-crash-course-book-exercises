#include <iostream>

/**
3-2. Add a read_from and a write_to function to Listing 3-6. 
These functions should read or write to upper or lower as appropriate. Perform bounds check-ing to prevent buffer overflows.
 * */

char ReadFrom(const int characterToRead, char *charList, const size_t charListSize) //3-2
{

    if (characterToRead < charListSize)
    {
        std::cout << charList[characterToRead] << std::endl;
        return charList[characterToRead];
    }
    return '0';
};

void WriteTo(const char character, const int location, char *charList, const size_t charListSize)
{
    if (location < charListSize)
    {
        charList[location] = character;
    }
};

int main()
{
    char lower[] = "abc?e";
    char upper[] = "ABC?E";

    char *upper_ptr = upper;

    WriteTo('d', 3, lower, sizeof(lower) / sizeof(lower[0]));
    WriteTo('D', 3, upper_ptr, sizeof(upper_ptr) / sizeof(upper_ptr[0]));

    char letter_d = ReadFrom(3, lower, sizeof(lower) / sizeof(lower[0]));
    char letter_D = ReadFrom(3, upper, sizeof(upper) / sizeof(upper[0]));

    return 0;
}