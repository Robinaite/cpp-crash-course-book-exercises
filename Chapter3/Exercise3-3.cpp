#include <iostream>

/**
3-3. Add an Element* previous to Listing 3-9 to make a doubly linked list. Add an insert_before method to Element. 
Traverse the list from front to back, then from back to front, using two separate for loops. 
Print the operating_numberinside each loop.
 * */

//the exercise asked for a for loop, but could've better used a while or a do while loop to loop over the whole list.

struct Element
{
    Element *next{};
    Element *previous{};
    void insert_after(Element *newElement)
    {
        newElement->next = next;     //sets the next element in newElement to what this element had
        newElement->previous = this; //sets the previous element of the new one to this one
        if (next)
        {
            next->previous = newElement; //sets the previous element of the next one of this element to the new one which got inserted
        }
        next = newElement; //changes this element next to the new element inserted
    }
    void insert_before(Element *newElement)
    {
        newElement->next = this;         //sets the next element of the new one to this element one.
        newElement->previous = previous; //sets the previous element to the previous of this one.
        if (previous)
        {
            previous->next = newElement; //sets the previous next element to the new element inserted before
        }
        previous = newElement; //sets previous to the new element
    }
    short operatingNumber;
};

int main()
{
    Element el1, el2, el3, el4;
    el1.operatingNumber = 1;
    el2.operatingNumber = 2;
    el3.operatingNumber = 3;
    el4.operatingNumber = 4;

    el1.insert_after(&el2);
    el2.insert_after(&el3);
    el3.insert_after(&el4);

    std::cout << "forward!" << std::endl;
    Element *elPointer = &el1;
    for (int i = 0; i < 4; i++) 
    {
        std::cout << elPointer->operatingNumber << std::endl;
        if (elPointer->next)
        {
            elPointer = elPointer->next;
        }
    }
    std::cout << "backwards!" << std::endl;
    for (int i = 0; i < 4; i++) 
    {
        std::cout << elPointer->operatingNumber << std::endl;
        elPointer = elPointer->previous;
    }

    Element el11, el22, el33, el44;
    el11.operatingNumber = 1;
    el22.operatingNumber = 2;
    el33.operatingNumber = 3;
    el44.operatingNumber = 4;

    el11.insert_before(&el22);
    el22.insert_before(&el33);
    el33.insert_before(&el44);

    std::cout << "forward!" << std::endl;
    elPointer = &el44;
    for (int i = 0; i < 4; i++)
    {
        std::cout << elPointer->operatingNumber << std::endl;
        if (elPointer->next)
        {
            elPointer = elPointer->next;
        }
    }
    std::cout << "backwards!" << std::endl;
    for (int i = 0; i < 4; i++) 
    {
        std::cout << elPointer->operatingNumber << std::endl;
        elPointer = elPointer->previous;
    }
    return 0;
}