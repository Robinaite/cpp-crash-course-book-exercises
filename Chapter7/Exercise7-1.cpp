#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <exception>
/*
7-1. Create an UnsignedBigInteger class that can handle numbers bigger than a long. 
You can use a byte array as the internal representation (for example, uint8_t[] or char[]). 
Implement operator overloads for operator+ and operator-. Perform runtime checks for overflow. 
For the intrepid, also implement operator*, operator/, and operator%. Make sure that your operator overloads work for both int types and UnsignedBigInteger types. 
Implement an operator int type conversion. Perform a runtime check if narrowing would occur.
*/

class UnsignedBigInteger
{
public:
    char *integerCharacters;
    size_t arrayLength;

    UnsignedBigInteger(const char *startValue, size_t length) : integerCharacters{new char[length]}, arrayLength{length}
    {
        if (std::strrchr(startValue, '-') != nullptr)
        {
            throw std::invalid_argument("No negative values allowed.");
        }

        std::strncpy(integerCharacters, startValue, length);
        integerCharacters[length] = *("\0");
        std::cout << "constructor: " << integerCharacters << std::endl;
    }
    ~UnsignedBigInteger()
    {
        delete[] integerCharacters;
    }

    UnsignedBigInteger operator+(const UnsignedBigInteger &other)
    {
        size_t lengthThis = arrayLength;
        size_t lengthOther = other.arrayLength;

        std::cout << integerCharacters << std::endl;

        size_t biggestLength = lengthThis > lengthOther ? lengthThis : lengthOther;

        int result[biggestLength];
        bool saveOnforNext = false;
        for (int i = biggestLength - 1; i >= 0; i--)
        {
            std::string str1;
            if (i < lengthThis)
            {
                str1 = integerCharacters[i];
            }
            else
            {
                str1 = "0";
            }
            std::string str2;
            if (i < lengthOther)
            {
                str2 = other.integerCharacters[i];
            }
            else
            {
                str2 = "0";
            }

            int resultVal = std::stoi(str1) + std::stoi(str2);
            if (saveOnforNext)
            {
                saveOnforNext = false;
                resultVal += 1;
            }

            if (resultVal >= 10)
            {
                result[i] = resultVal % 10;
                saveOnforNext = true;
            }
            else
            {
                result[i] = resultVal;
            }
        }

        std::string newStr;

        if (saveOnforNext)
        {
            newStr = std::to_string(1);
        }

        for (size_t i = 0; i < biggestLength; i++)
        {
            newStr += std::to_string(result[i]);
        }

        std::cout << newStr << std::endl;

        return UnsignedBigInteger{newStr.c_str(), std::strlen(newStr.c_str())};
    }

    UnsignedBigInteger operator-(const UnsignedBigInteger &other)
    {
        size_t lengthThis = strlen(integerCharacters);
        size_t lengthOther = strlen(other.integerCharacters);

        if (lengthThis < lengthOther)
        {
            throw std::invalid_argument("The first value is shorter than the second value, will create negative value.");
        }

        size_t biggestLength = lengthThis > lengthOther ? lengthThis : lengthOther;

        int result[biggestLength];
        bool removeOneFromNext = false;
        for (int i = biggestLength - 1; i >= 0; i--)
        {
            std::string str1;
            if (i < lengthThis)
            {
                str1 = integerCharacters[i];
            }
            else
            {
                str1 = "0";
            }
            std::string str2;
            if (i < lengthOther)
            {
                str2 = other.integerCharacters[i];
            }
            else
            {
                str2 = "0";
            }
            int a = std::stoi(str1);
            if (removeOneFromNext)
            {
                a -= 1;
                removeOneFromNext = false;
            }

            int b = std::stoi(str2);
            if (a < b)
            {
                a += 10;
                removeOneFromNext = true;
            }

            result[i] = a - b;
        }

        std::string newStr;
        bool removedInitialZeros = false;
        for (size_t i = 0; i < biggestLength; i++)
        {
            if (!removedInitialZeros && result[i] != 0)
            {
                removedInitialZeros = true;
            }

            if (removedInitialZeros)
            {
                newStr += std::to_string(result[i]);
            }
        }

        std::cout << newStr << std::endl;

        return UnsignedBigInteger{newStr.c_str(), std::strlen(newStr.c_str())};
    }

    UnsignedBigInteger operator*(const UnsignedBigInteger &other)
    {
        size_t lengthThis = strlen(integerCharacters);
        size_t lengthOther = strlen(other.integerCharacters);

        int results[lengthOther][lengthThis + 1];

        for (int i = lengthOther - 1; i >= 0; i--)
        {

            std::string str2{other.integerCharacters[i]};
            int amountForNext = 0;
            for (int j = lengthThis; j > 0; j--)
            {
                std::string str1{integerCharacters[j - 1]};

                int resultVal = std::stoi(str1) * std::stoi(str2);
                if (amountForNext > 0)
                {
                    resultVal += amountForNext;
                    amountForNext = 0;
                }

                if (resultVal >= 10)
                {
                    results[i][j] = resultVal % 10;
                    amountForNext = resultVal / 10;
                }
                else
                {
                    results[i][j] = resultVal;
                }
            }
            results[i][0] = amountForNext;
        }

        std::string str[lengthOther];
        UnsignedBigInteger valTotal{"0", 1};
        for (int i = 0; i < lengthOther; i++)
        {
            for (int j = 0; j <= lengthThis; j++)
            {
                str[i] += std::to_string(results[lengthOther - i - 1][j]);
            }
            for (int extra0 = 0; extra0 < i; extra0++)
            {
                str[i] += std::to_string(0);
            }
            UnsignedBigInteger valToAdd{str[i].c_str(), std::strlen(str[i].c_str())};
            //Misses addition to get ValTotal, for some reason its not working as the second time the integerCharacters array is empty.
        }

        std::cout << valTotal.integerCharacters << std::endl;

        return valTotal;
    }
};

int main()
{
    UnsignedBigInteger test{"522", 3};
    UnsignedBigInteger test2{"22", 2};

    UnsignedBigInteger test3 = test - test2;
    UnsignedBigInteger test4 = test + test2;
    //UnsignedBigInteger test5 = test * test2; Not working 100%, missing last step, but having issues with pointers dissapearing. :|
    return 0;
}