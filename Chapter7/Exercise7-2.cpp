#include <iostream>
#include <new>
#include <cstddef>

/*
7-2. Create a LargeBucket class that can store up to 1MB of data. Extend the Heap class so it gives out a LargeBucket for allocations greater than 4096 bytes. 
Make sure that you still throw std::bad_alloc whenever the Heap is unable to allocate an appropriately sized bucket.
*/

struct Bucket
{
    const static size_t data_size{4096};
    std::byte data[data_size];
};

struct LargeBucket
{
    const static size_t data_size{1000000};
    std::byte data[data_size];
};

struct Heap
{
    void *Allocate(size_t bytes)
    {
        if (bytes > Bucket::data_size)
        {
            if (bytes > LargeBucket::data_size)
            {
                throw std::bad_alloc{};
            }
            return AllocateLargeBucket();
        }
        return AllocateBucket();

        throw std::bad_alloc{};
    }

    void Free(void *p)
    {
        for (size_t i{}; i < nHeapBuckets; i++)
        {
            if (buckets[i].data == p)
            {
                bucketUsed[i] = false;
                return;
            }
        }
    }

    static const size_t nHeapBuckets{10};
    Bucket buckets[nHeapBuckets]{};
    bool bucketUsed[nHeapBuckets]{};

    LargeBucket largeBuckets[nHeapBuckets]{};
    bool largeBucketUsed[nHeapBuckets]{};

private:
    void *AllocateBucket()
    {
        for (size_t i{}; i < nHeapBuckets; i++)
        {
            if (!bucketUsed[i])
            {
                bucketUsed[i] = true;
                return buckets[i].data;
            }
        }
        throw std::bad_alloc{};
    }
    void *AllocateLargeBucket()
    {
        for (size_t i{}; i < nHeapBuckets; i++)
        {
            if (!largeBucketUsed[i])
            {
                largeBucketUsed[i] = true;
                return largeBuckets[i].data;
            }
        }
        throw std::bad_alloc{};
    }
};

int main()
{

    return 0;
}