#include <cstdio>
#include <memory>

/**
11-1. Reimplement Listing 11-13 to use a std::shared_ptr rather than a std::unique_ptr. 
Notice that although you’ve relaxed the ownership require-ments from exclusive to non-exclusive, you’re still transferring ownership to the say_hello function.
11-2. Remove the std::move from the call to say_hello. Then make an addi-tional call to say_hello. 
Notice that the ownership of file_guard is no longer transferred to say_hello. This permits multiple calls
11-3. Implement a Hal class that accepts a std::shared_ptr<FILE> in its con-structor. 
In Hal’s destructor, write the phrase Stop, Dave. to the file handle held by your shared pointer. 
Implement a write_status function that writes the phrase I'm completely operational. to the file handle. 
Here’s a class declaration you can work from:
struct Hal {  
  Hal(std::shared_ptr<FILE> file);
  ~Hal();
  void write_status();
  std::shared_ptr<FILE> file;
};
11-4. Create several Hal instances and invoke write_status on them. 
Notice that you don’t need to keep track of how many Hal instances are open: file management gets handled via the shared pointer’s shared ownership model.
 * */


using FileGuard = std::shared_ptr<FILE>;

struct Hal {  
  Hal(std::shared_ptr<FILE> file){
    this->file = file;
  }
  ~Hal(){
    fprintf(file.get(), "Stop, DAVE");
  }
  void write_status(){
    fprintf(file.get(), "I'm completely operational");
  }
  std::shared_ptr<FILE> file;
};

void say_hello(FileGuard file) {
  fprintf(file.get(), "HELLO DAVE");
};

int main() {
  auto file = fopen("HAL9000", "w");
  if(!file)
    return errno;
  FileGuard file_guard{ file, fclose };
  // File open here
  say_hello(file_guard);
  // File closed here
  say_hello(file_guard);

  Hal hal1{file_guard};
  Hal hal2{file_guard};
  Hal hal3{file_guard};

  hal1.write_status();
  hal2.write_status();
  hal3.write_status();
  return 0;
}