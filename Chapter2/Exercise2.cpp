#include <iostream>

/**
 * 2-1. Create an enum class Operation that has values Add, Subtract, Multiply, and Divide.
 * 2-2. Create a struct Calculator. It should have a single constructor that takes an Operation.
 * 2-3. Create a method on Calculator called int calculate(int a, int b). Upon invocation, this method should 
 * perform addition, subtraction, multiplica-tion, or division based on its constructor argument and return the result.
 * 2-4. Experiment with different means of initializing Calculator instances
 * */

enum class Operation//2-1
{ 
    Add,
    Subtract,
    Multiply,
    Divide
};

struct Calculator
{
    Operation operation;

    Calculator(Operation operation)//2-2
    { 
        this->operation = operation;
    }

    int Calculate(int a, int b) //2-3
    {
        switch (operation)
        {
        case Operation::Add:
            return a + b;
        case Operation::Subtract:
            return a - b;
        case Operation::Multiply:
            return a * b;
        case Operation::Divide:
            if (b > 0)
            {
                return a / b;
            }
            else
            {
                return 0;
            }
        default:
            return 0;
        }
    }
};

int main()
{
    Calculator cal1(Operation::Add); //2-4
    Calculator cal2{ Operation::Add };
    Calculator cal3{ Operation::Divide };
    Calculator cal4{ Operation::Multiply };
    Calculator cal5{ Operation::Subtract };

    std::cout << cal1.Calculate(10,5) << std::endl;
    std::cout << cal2.Calculate(10,5) << std::endl;
    std::cout << cal3.Calculate(10,5) << std::endl;
    std::cout << cal4.Calculate(10,5) << std::endl;
    std::cout << cal5.Calculate(10,5) << std::endl;

    return 0;
}