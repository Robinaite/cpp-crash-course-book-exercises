#include <iostream>
#include <map>

/*
9-2. Implement a program that accepts an arbitrary number of command line arguments, counts the length in characters of each argument, 
and prints a histo-gram of the argument length distribution.
*/

struct Histogram
{
    void CountCharacters(char *argument)
    {
        size_t charCount{};
        while (const auto ch = argument[charCount])
        {
            charCount++;
        }
        if (lengthCount.count(charCount) > 0)
        {
            lengthCount[charCount]++;
        }
        else
        {
            lengthCount[charCount] = 1;
        }
    }

    void Print()
    {
        for (std::pair<int, int> key : lengthCount)
        {
            std::cout << key.first << " : ";
            for (size_t i{0}; i < key.second; i++)
            {
                std::cout << "*";
            }
            std::cout << "\n";
        }
    }

private:
    std::map<int, int> lengthCount;
};

int main(int argc, char **argv)
{
    Histogram hist{};
    for (size_t i{1}; i < argc; i++)
    {
        hist.CountCharacters(argv[i]);
    }
    hist.Print();
}