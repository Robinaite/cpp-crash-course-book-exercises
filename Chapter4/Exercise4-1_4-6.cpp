#include <iostream>
#include <sys/time.h>
#include <unistd.h> //Only on LINUX!
/**
4-1. Create a struct TimerClass. In its constructor, record the current time in a field called timestamp (compare with the POSIX function gettimeofday).
4-2. In the destructor of TimerClass, record the current time and subtract the time at construction. This time is roughly the age of the timer. Print this value.
4-3. Implement a copy constructor and a copy assignment operator for TimerClass. The copies should share timestamp values.
4-4. Implement a move constructor and a move assignment operator for TimerClass. A moved-from TimerClass shouldn’t print any output to the console when it gets destructed.
4-5. Elaborate the TimerClass constructor to accept an additional const char* name parameter. When TimerClass is destructed and prints to stdout, include the name of the timer in the output.
4-6. Experiment with your TimerClass. Create a timer and move it into a func-tion that performs some computationally intensive operation (for example, lots of math in a loop). Verify that your timer behaves as you expect.
* */

struct TimerClass
{
    TimerClass(const char *name)
    {
        gettimeofday(&timestamp, nullptr);
        timerName = name;
    }
    TimerClass(const TimerClass &other)
    { //Copy constructor
        timestamp = other.timestamp;
        timerName = other.timerName;
    }
    TimerClass(TimerClass &&other)
    {
        timestamp = other.timestamp;
        other.timestamp.tv_sec = -1;
        other.timestamp.tv_usec = -1;
        timerName = other.timerName;
    }

    TimerClass &operator=(const TimerClass &other)
    {
        if (this == &other)
        {
            return *this;
        }
        timestamp = other.timestamp;
        timerName = other.timerName;
        return *this;
    }

    TimerClass &operator=(TimerClass &other)
    {
        if (this == &other)
        {
            return *this;
        }
        timestamp = other.timestamp;
        timerName = other.timerName;
        other.timestamp.tv_sec = -1;
        other.timestamp.tv_usec = -1;

        return *this;
    }

    ~TimerClass()
    {
        if (timestamp.tv_sec >= 0)
        {
            timeval timeOnDest;
            gettimeofday(&timeOnDest, nullptr);

            std::cout << timerName << " - Seconds: " << timeOnDest.tv_sec - timestamp.tv_sec << std::endl;
        }
    }

private:
    timeval timestamp;
    const char *timerName;
};

int main()
{
    TimerClass timer{"test"};

    TimerClass timer25{"test2"};
    TimerClass timer3{std::move(timer25)};
    sleep(2);

    TimerClass timer2{timer};
    return 0;
}