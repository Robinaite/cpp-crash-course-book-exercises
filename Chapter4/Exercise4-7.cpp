#include <iostream>
#include <cstring>

/**
4-7. Identify each method in the SimpleString class (Listing 4-38). Try reimple-menting it from scratch without referring to the book.
 * */

struct SimpleString
{
    //Constructor
    SimpleString(const size_t max_size) : max_size{max_size}
    {
        if (max_size == 0)
        {
            throw std::runtime_error("Max size was too small.");
        }
        buffer = new char[max_size]{0};
    }
    //destructor
    ~SimpleString()
    {
        delete[] buffer;
    }
    //copy constr
    SimpleString(const SimpleString &other) : max_size{other.max_size}, buffer{new char[max_size]}
    {
        std::strncpy(buffer, other.buffer, max_size);
    }
    //move const
    SimpleString(SimpleString &&other) noexcept : max_size{other.max_size}
    {
        buffer = other.buffer;
        other.buffer = nullptr;
    }
    //copy oper
    SimpleString &operator=(const SimpleString &other)
    {
        if (this == &other)
        {
            return *this;
        }
        max_size = other.max_size;
        buffer = new char[max_size];
        std::strncpy(buffer, other.buffer, max_size);
        return *this;
    }
    //move operatr
    SimpleString &operator=(SimpleString &&other) noexcept
    {
        if (this == &other)
        {
            return *this;
        }
        max_size = other.max_size;
        buffer = other.buffer;
        max_size = 0;
        buffer = nullptr;
    }
    //print
    void Print()
    {
        if (buffer)
        {
            std::cout << buffer << std::endl;
        }
    }
    //append_line
    bool AppendLine(const char *line)
    {
        int lengthLine = strlen(line);
        int lengthBuffer = strlen(buffer);
        if (lengthLine + lengthBuffer + 2 > max_size)
        {
            return false;
        }
        std::strncpy(buffer + lengthBuffer, line, max_size - lengthBuffer);
        lengthBuffer += lengthLine;
        buffer[lengthBuffer] = '\0';
        return true;
    }

private:
    size_t max_size;
    char *buffer;
};

int main()
{
    SimpleString test1{100};
    std::cout << test1.AppendLine("Test") << std::endl;
    std::cout << test1.AppendLine("33333") << std::endl;
    test1.Print();

    SimpleString test2{test1};
    test2.Print();

    SimpleString test3{std::move(test1)};
    test3.Print();
    test1.Print();

    SimpleString test4{100};
    test4.AppendLine("Test2");
    test4.AppendLine("5555");
    test4.Print();

    SimpleString test5 = test4;
    test5.Print();

    SimpleString test6 = std::move(test4);
    test6.Print();
    test4.Print();

    return 0;
}