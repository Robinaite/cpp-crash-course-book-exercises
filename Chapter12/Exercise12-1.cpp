/**
12-1. Reimplement the narrow_cast in Listing 6-6 to return a std::optional. 
If the cast would result in a narrowing conversion, return an empty optional rather than throwing an exception. Write a unit test that ensures your solution works.
 * */

#include <stdexcept>
#include <optional>


void assert_that(bool statement, const char *message)
{
    if (!statement)
        throw std::runtime_error(message);
}

int run_test(void (*unit_test)(), const char *name)
{
    try
    {
        unit_test();
        printf("[+] Test %s successful.\n", name);
        return 0;
    }
    catch (const std::exception &e)
    {
        printf("[-] Test failure in %s. %s.\n", name, e.what());
    }
    return -1;
}


template <typename To, typename From>
std::optional<To> narrowCast(From value)
{
  const auto converted = static_cast<To>(value);
  const auto backwards = static_cast<From>(converted);
  if (value != backwards)
  {
    return std::nullopt;
  }
  return converted;
};

void IsNarrowing(){
  std::optional<int> test = narrowCast<int,double>(10000000000000000000ULL);
  if(!test){
    assert_that(true,"Narrowing happened");
  }
}

int main()
{
  run_test(IsNarrowing,"Narrowing test");
  return 0;
}