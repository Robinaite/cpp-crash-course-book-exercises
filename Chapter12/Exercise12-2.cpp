/**
12-2. Implement a program that generates random alphanumeric passwords and writes them to the console. 
You can store the alphabet of possible characters into a char[] and use the discrete uniform distribution with a minimum of zero and 
a maximum of the last index of your alphabet array. Use a cryptographically secure random number engine.
 * */

#include <iostream>
#include <random>
#include <cstring>


int main()
{
  char charactersToBeUsed[] = "ABCDEFGHIJKLMNOPQRSTUVWYXZ1234567890";
  std::uniform_int_distribution<int> intD{0,std::strlen(charactersToBeUsed)};
  std::random_device rndDev{};
  for(size_t i{};i<10;i++){
    std::cout << charactersToBeUsed[intD(rndDev)];
  }
  std::cout << std::endl;
  return 0;
}