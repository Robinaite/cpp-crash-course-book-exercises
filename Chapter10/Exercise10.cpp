#include <cstdio>
#include <functional>
#include <stdexcept>

/**
 * 10 -1. Your car company has completed work on a service that detects speed limits based on signage it observes on the side of the road. 
 * The speed-limit-detection team will publish objects of the following type to the event bus periodically:
 * The service bus has been extended to incorporate this new type:
 * Update the service with the new interface and make sure the tests still pass. 
 * 10-2. Add a private field for the last known speed limit. Implement a getter method for this field.
 * 10-3. The product owner wants you to initialize the last known speed limit to 39 meters per second. 
 * Implement a unit test that checks a newly constructed AutoBrake that has a last known speed limit of 39.
 * 10-4. Make unit tests pass.
 * 10-5. Implement a unit test where you publish three different SpeedLimitDetected objects using the same callback technique you used for SpeedUpdate and CarDetected. 
 * After invoking each of the callbacks, check the last known speed limit on the AutoBrake object to ensure it matches.
 * 10-6. Make all unit tests pass.
 * 10-7. Implement a unit test where the last known speed limit is 35 meters per second, and you’re traveling at 34 meters per second.
 * Ensure that no BrakeCommand is published by AutoBrake.
 * 10-8. Make all unit tests pass.
 * */

using namespace std;

struct SpeedLimitDetected
{
    unsigned short speedLimit_mps;
};

struct SpeedUpdate
{
    double velocity_mps;
};

struct CarDetected
{
    double distance_m;
    double velocity_mps;
};

struct BreakCommand
{
    double time_to_collision_s;
};

typedef function<void(const SpeedUpdate &)> SpeedUpdateCallback;
typedef function<void(const CarDetected &)> CarDetectedCallback;
typedef function<void(const SpeedLimitDetected &)> SpeedLimitCallback;

struct IServiceBus
{
    virtual ~IServiceBus() = default;
    virtual void publish(const BreakCommand &) = 0;
    virtual void subscribe(SpeedUpdateCallback) = 0;
    virtual void subscribe(CarDetectedCallback) = 0;
    virtual void subscribe(SpeedLimitCallback) = 0;
};

struct MockServiceBus : IServiceBus
{
    void publish(const BreakCommand &cmd) override
    {
        commands_published++;
        last_command = cmd;
    };
    void subscribe(SpeedUpdateCallback callback) override
    {
        speed_update_callback = callback;
    };
    void subscribe(CarDetectedCallback callback) override
    {
        car_detected_callback = callback;
    };
    void subscribe(SpeedLimitCallback callback) override
    {
        speedLimitCallback = callback;
    };

    BreakCommand last_command{};
    int commands_published{};
    SpeedUpdateCallback speed_update_callback{};
    CarDetectedCallback car_detected_callback{};
    SpeedLimitCallback speedLimitCallback{};
};

struct AutoBrake
{
    AutoBrake(IServiceBus &bus)
        : collision_threshold_s{5}, speed_mps{}, lastSpeedLimit{39}
    {
        bus.subscribe([this](const SpeedUpdate &update) {
            if (update.velocity_mps < lastSpeedLimit)
            {
                speed_mps = update.velocity_mps;
            }
        });
        bus.subscribe([this, &bus](const CarDetected &cd) {
            auto relative_velocity_mps = speed_mps - cd.velocity_mps;
            auto time_to_collision_s = cd.distance_m / relative_velocity_mps;
            if (time_to_collision_s > 0 && time_to_collision_s <= collision_threshold_s)
            {
                bus.publish(BreakCommand{time_to_collision_s});
            }
        });
        bus.subscribe([this, &bus](const SpeedLimitDetected &spLim) {
            lastSpeedLimit = spLim.speedLimit_mps;
            if (speed_mps > lastSpeedLimit)
            {
                bus.publish(BreakCommand{});
            }
        });
    }
    void set_collision_threshold_s(double x)
    {
        if (x < 1)
            throw runtime_error{"Collision less than 1."};
        collision_threshold_s = x;
    }
    double get_collision_threshold_s() const
    {
        return collision_threshold_s;
    }
    double get_speed_mps() const
    {
        return speed_mps;
    }
    double GetLastSpeedLimit() const
    {
        return lastSpeedLimit;
    }

private:
    double collision_threshold_s;
    double speed_mps;
    double lastSpeedLimit;
};

void assert_that(bool statement, const char *message)
{
    if (!statement)
        throw runtime_error(message);
}

int run_test(void (*unit_test)(), const char *name)
{
    try
    {
        unit_test();
        printf("[+] Test %s successful.\n", name);
        return 0;
    }
    catch (const exception &e)
    {
        printf("[-] Test failure in %s. %s.\n", name, e.what());
    }
    return -1;
}

void initial_speed_is_zero()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    assert_that(auto_break.get_speed_mps() == 0, "speed not equal 0");
}

void initial_sensitivity_is_five()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    assert_that(auto_break.get_collision_threshold_s() == 5, "sensitivity is not 5");
}

void sensitivity_greater_than_1()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    try
    {
        auto_break.set_collision_threshold_s(0.5L);
    }
    catch (const exception &)
    {
        return;
    }
    assert_that(false, "no exception thrown");
}

void speed_is_saved()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};

    bus.speed_update_callback(SpeedUpdate{100L});
    assert_that(100L == auto_break.get_speed_mps(), "speed was not saved to 100");
    bus.speed_update_callback(SpeedUpdate{50L});
    assert_that(50L == auto_break.get_speed_mps(), "speed was not saved to 50");
    bus.speed_update_callback(SpeedUpdate{0L});
    assert_that(0L == auto_break.get_speed_mps(), "speed was not saved to 0");
}

void no_alert_when_not_imminent()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    auto_break.set_collision_threshold_s(2L);
    bus.speed_update_callback(SpeedUpdate{100L});
    bus.car_detected_callback(CarDetected{1000L, 50L});
    assert_that(bus.commands_published == 0, "break commands were published");
}

void alert_when_imminent()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    auto_break.set_collision_threshold_s(10L);
    bus.speed_update_callback(SpeedUpdate{100L});
    bus.car_detected_callback(CarDetected{100L, 0L});
    assert_that(bus.commands_published == 1, "1 break command was not published");
    assert_that(bus.last_command.time_to_collision_s == 1L,
                "time to collision"
                "not computed correctly.");
}

void InitialLastKnownSpeedLimit()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    assert_that(auto_break.GetLastSpeedLimit() == 39, "Last speed Limit is not 39");
}

void SpeedLimistIsSaved()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};

    bus.speedLimitCallback(SpeedLimitDetected{100});
    assert_that(100 == auto_break.GetLastSpeedLimit(), "speed limit was not saved to 100");
    bus.speedLimitCallback(SpeedLimitDetected{50});
    assert_that(50 == auto_break.GetLastSpeedLimit(), "speed limit was not saved to 50");
    bus.speedLimitCallback(SpeedLimitDetected{0});
    assert_that(0 == auto_break.GetLastSpeedLimit(), "speed limit was not saved to 0");
}

void NoBreakAlertIfBelowSpeedLimit()
{
    MockServiceBus bus{};
    AutoBrake auto_break{bus};
    bus.speedLimitCallback(SpeedLimitDetected{35});
    bus.speed_update_callback(SpeedUpdate{34});
    assert_that(bus.commands_published == 0, "break commands were published");
}

int main()
{
    run_test(initial_speed_is_zero, "initial speed is 0");
    run_test(initial_sensitivity_is_five, "initial sensitivity is 5");
    run_test(sensitivity_greater_than_1, "sensitivity greater than 1");
    run_test(speed_is_saved, "speed is saved");
    run_test(no_alert_when_not_imminent, "no alert when not imminent");
    run_test(alert_when_imminent, "alert when imminent");

    //Exercises
    run_test(InitialLastKnownSpeedLimit, "initial speed limit is 39");
    run_test(SpeedLimistIsSaved, "speed limit is saved");
    run_test(NoBreakAlertIfBelowSpeedLimit, "No Alert if below speed limit");
}