#include "PrimeNumberRange.hpp"

bool PrimeNumberIterator::operator!=(int x) const
{
    return x >= current;
}

PrimeNumberIterator &PrimeNumberIterator::operator++()
{

    bool isPrime = true;
    do
    {
        current++;
        isPrime = true;
        for (int i = 2; i < current; i++)
        {
            if (current % i == 0)
            {
                isPrime = false;
                break;
            }
        }
    } while (!isPrime);
    return *this;
}

PrimeNumberRange::PrimeNumberRange(int max) : max{max} {}

int PrimeNumberIterator::operator*() const
{
    return current;
}

PrimeNumberIterator PrimeNumberRange::begin() const
{
    return PrimeNumberIterator{};
}

int PrimeNumberRange::end() const
{
    return max;
}