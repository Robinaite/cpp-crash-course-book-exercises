#include "Fibonacci.hpp"

bool FibonacciIterator::operator!=(int x) const
{
    return x >= current;
}

FibonacciIterator &FibonacciIterator::operator++()
{
    const auto tmp = current;
    current += last;
    last = tmp;
    return *this;
}

FibonacciRange::FibonacciRange(int max) : max{max} {}

int FibonacciIterator::operator*() const
{
    return current;
}

FibonacciIterator FibonacciRange::begin() const
{
    return FibonacciIterator{};
}

int FibonacciRange::end() const
{
    return max;
}