#include <iostream>

/**
 *   6-5 till 6-7 is not possible when using my implementation of bank from exercises due to templates not being able to work with polymorphism well.
As such u can find a new version of bank in these exercises

Also, its not logical to make Bank<Account> a its impossible to access account methods in a generic method, except if I cast it to account. 
If its like this wouldn't it just be better to not use generic programming and just templates?
*/


/**

6-5. Using the example from Chapter 5, make Bank a template class that accepts a template parameter. 
Use this type parameter as the type of an account rather than long. Verify that your code still works using a Bank<long>class.
6-6. Implement an Account class and instantiate a Bank<Account>. Implement functions in Account to keep track of balances.
6-7. Make Account an interface. Implement a CheckingAccount and SavingsAccount. 
Create a program with several checking and savings accounts. Use a Bank<Account> to make several transactions between the accounts.
* */

struct Account
{
    virtual ~Account() = default;
    virtual void AddToBalance(double amount) = 0;
    virtual double ObtainBalance() = 0;
    virtual long GetID() = 0;
};

struct CheckingAccount : Account
{
    CheckingAccount(long id) : id{id}
    {

    }
    void AddToBalance(double amount)
    {
        balance = +amount;
    }

    double ObtainBalance()
    {
        return balance;
    }
    long GetID()
    {
        return id;
    }

private:
    long id;
    double balance;
};

struct SavingAccount : Account
{
    SavingAccount(long id) : id{id}
    {

    }
    void AddToBalance(double amount)
    {
        balance = +amount;
    }

    double ObtainBalance()
    {
        return balance;
    }
    long GetID()
    {
        return id;
    }

private:
    long id;
    double balance;
};

template <typename T>
struct ConsoleLogger
{
    void log_transfer(T &from, T &to, double amount)
    {
        //std::cout << from << " -> " << to << ": " << amount << std::endl;
    }
};

template <typename T>
struct Bank
{
    void make_transfer(T &from, T &to, double amount)
    {
        //process transfer, how do I access the account methods in here?? 
        //Only possible if we cast T to account, but then its pretty useless to use templates???
        logger.log_transfer(from, to, amount);
    }
    ConsoleLogger<T> logger;
};

int main()
{
    Bank<Account> bank;

    CheckingAccount ch1{1};
    CheckingAccount ch2{2};
    SavingAccount sv1{3};
    SavingAccount sv2{4};

    bank.make_transfer(ch1, ch2, 49.95);
    bank.make_transfer(sv1, sv2, 20.00);
}