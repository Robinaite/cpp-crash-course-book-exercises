#include <iostream>
#include <map>
/**
6-1. The mode of a series of values is the value that appears most commonly. 
Implement a mode function using the following signature: int mode(const int* values, size_t length). 
If you encounter an error condition, such as input having multiple modes and no values, return zero.
6-2. Implement mode as a template function.
6-3. Modify mode to accept an Integer concept. Verify that mode fails to instantiate with floating types like double. //No Access to Concepts unfortunately
6-4. Refactor mean in Listing 6-13 to accept an array rather than pointer and length arguments. Use Listing 6-33 as a guide.
6-5 till 6-7 is not possible when using my implementation of bank from exercises due to templates not being able to work with polymorphism well.
As such u can find a new version of bank in these exercises
* */

/* As concepts are not available yet on my compiler version I can only approximately make it work.
template<typename T>
concept bool Integer(){
    return std::is_integral<T>::value;
};
*/
template <typename T> //If using concepters here I should substitute typeame with the respective Concept.
T mode(const T *values, size_t length)
{

    std::pair<T, int> highestElement{0, 0};
    bool moreThanOne = false;
    std::map<T, int> amounts{};

    for (size_t i{}; i < length; i++)
    {
        if (amounts.count(values[i]) > 0)
        {
            amounts[values[i]]++;
        }
        else
        {
            amounts[values[i]] = 1;
        }

        if (amounts[values[i]] > highestElement.second)
        {
            highestElement.first = values[i];
            highestElement.second = amounts[values[i]];
            moreThanOne = false;
        }
        else if (amounts[values[i]] == highestElement.second)
        {
            moreThanOne = true;
        }
    }

    return moreThanOne ? 0 : highestElement.first;
};

template <typename T, size_t length>
T mean(const T (&values)[length])
{
    T result{};
    for (size_t i{}; i < length; i++)
    {
        result += values[i];
    }
    return result / length;
}

int main()
{
    const float values[]{1.3f, 1.3f, 5, 2, 6};
    std::cout << mode(values, 5) << std::endl;

    std::cout << mean(values) << std::endl;

    return 0;
}